export default [
  {
    path: '/',
    component: '@/pages/index/index',
    name: '工作台',
    icon: 'DashboardOutlined',
  },
  { 
    path: '/article',
    name: '文章管理',
    icon: 'FormOutlined',
    routes: [
      {
        path: '/article/articles',
        component: '@/pages/article/articles',
        name: '所有文章',
        icon: 'FormOutlined',
      },
      {
        path: '/article/category',
        component: '@/pages/article/category',
        name: '分类管理',
        icon: 'CopyOutlined',
      },
      {
        path: '/article/tags',
        component: '@/pages/article/tags',
        name: '标签管理',
        icon: 'TagOutlined',
      },
    ],
  },
  {
    path: '/page',
    component: '@/pages/page/index',
    name: '页面管理',
    icon: 'SnippetsOutlined',
  },
  {
    path: '/knowledge',
    component: '@/pages/knowledge/index',
    name: '知识小册',
    icon: 'BookOutlined',
  },
  {
    path: '/poster',
    component: '@/pages/poster/index',
    name: '海报管理',
    icon: 'StarOutlined',
  },
  {
    path: '/comment',
    component: '@/pages/comment/index',
    name: '评论管理',
    icon: 'MessageOutlined',
  },
  {
    path: '/mail',
    component: '@/pages/mail/index',
    name: '邮件管理',
    icon: 'MailOutlined',
  },
  {
    path: '/file',
    component: '@/pages/file/index',
    name: '文件管理',
    icon: 'FolderOpenOutlined',
  },
  {
    path: '/search',
    component: '@/pages/search/index',
    name: '搜索记录',
    icon: 'SearchOutlined',
  },
  {
    path: '/view',
    component: '@/pages/view/index',
    name: '访问统计',
    icon: 'ProjectOutlined',
  },
  {
    path: '/user',
    component: '@/pages/user/index',
    name: '用户管理',
    icon: 'UserOutlined',
  },
  {
    path: '/setting',
    component: '@/pages/setting/index',
    name: '系统设置',
    icon: 'SettingOutlined',
  },
  {
    path: '/register',
    component: '@/pages/register/',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    path: '/login',
    component: '@/pages/login/',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
];
