import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes: routes,
  layout: {
    name:"八维创作平台",
    logo:"https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp3.itc.cn%2Fq_70%2Fimages03%2F20200827%2F1d7129789e554980a33b4b841ed3e703.png&refer=http%3A%2F%2Fp3.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1666597625&t=0af275e075f508d56cd9cd02f9e0e131"
  },
});
